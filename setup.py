#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

setup(name='PyXMCDA',
      version='1.0',
      description='Python XMCDA library',
      author='Thomas Veneziano',
      maintainer='Sébastien Bigaret',
      maintainer_email='sebastien.bigaret@telecom-bretagne.eu',
      license='CeCILL v2 http://www.cecill.info/index.en.html',
      py_modules=['PyXMCDA'],
      install_requires=[ 'lxml', ],
     )
